module gitlab.com/felipemocruha/echo-pprof

go 1.14

require (
	github.com/labstack/echo/v4 v4.1.16
	github.com/labstack/gommon v0.3.0
	github.com/mattn/go-colorable v0.1.6
	github.com/mattn/go-isatty v0.0.12
	github.com/valyala/bytebufferpool v1.0.0
	github.com/valyala/fasttemplate v1.1.0
	golang.org/x/crypto v0.0.0-20200221231518-2aa609cf4a9d
	golang.org/x/sys v0.0.0-20200223170610-d5e6a3e2c0ae
)
